export interface EnvironmentModel  {
    production: boolean;
    here_map_app_id: string;
    here_map_app_code: string;
}
