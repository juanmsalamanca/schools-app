import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ListItem } from '../../constants';

@Component({
    selector: 'app-preview-info',
    templateUrl: './preview-info.component.html',
    styleUrls: ['./preview-info.component.scss'],
})
export class PreviewInfoComponent implements OnInit, OnChanges {

    @Input()
    public data: any;

    @Input()
    public mapLabelsInfo: Map<string, string>;

    @Output()
    private closeEmitter = new EventEmitter<boolean>();

    public itemList: ListItem[] = [];
    public isFull: boolean;

    private labelsMap = new Map<string, string>();

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log('data prev changes', this.data);
        this.itemList = [];
        if (!!this.data && !!this.data.properties) {
            Object.keys(this.data.properties).forEach(key => {
                const label = this.mapLabelsInfo.get(key);

                if (!!label && !!this.data.properties[key]) {
                    this.itemList.push({
                        label,
                        value: this.data.properties[key]
                    });
                }

            });
        }

    }

    onCloseClick(): void {
        this.isFull = false;
        this.closeEmitter.emit(true);
    }

    showFull(): void {
        this.isFull = !this.isFull;

    }

}
