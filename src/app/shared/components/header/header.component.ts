import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {

    @Input()
    public title = 'Title';

    @Output()
    public filterEmitter = new EventEmitter<void>();

    @Output()
    public centerEmitter = new EventEmitter<void>();

    constructor(private location: Location) {
    }

    public goBack(): void {
        this.location.back();
    }

}
