import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import * as CONST from '../../constants';
import { SchoolStoreFacade } from '../../../apps/schools/store/school.store.facade';
import { FilterI } from '../../../apps/schools/store/school.store.state';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit, OnDestroy {

    private filterSubscription: Subscription;

    @Output()
    private closeEmitter = new EventEmitter<boolean>();

    public localities = CONST.LOCALITIES;

    public properties: FilterI = {
        locality: null,
        disability: false
    };


    constructor(private schoolStoreFacade: SchoolStoreFacade) {
    }

    ngOnInit() {
        this.filterSubscription = this.schoolStoreFacade.getFilters$.subscribe((value: FilterI) => {
            this.properties = {...value};
        });
    }

    ngOnDestroy() {
        this.filterSubscription.unsubscribe();
    }


    onCloseClick() {
        this.schoolStoreFacade.changeFilter(this.properties);
        this.closeEmitter.emit(true);
    }

}
