import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MapService } from '../here-map/services/map.service';
import { ToastController } from '@ionic/angular';
import { Geolocation } from '@capacitor/core';
import { Observable, Subscription } from 'rxjs';

export interface InitialSettings {
    title: string;
    type: 'police-stations' | 'schools' | 'CAIS';
    iconForMarker: string;
    mapLabelsInfo: Map<string, string>;
    dataObservable: Observable<any>;

}


@Component({
    selector: 'app-generic-map-view',
    templateUrl: 'generic-map-view.component.html',
    styleUrls: ['generic-map-view.component.scss'],
})
export class GenericMapViewComponent implements OnInit, OnDestroy {

    @Input()
    public initialSettings: InitialSettings;

    private map;
    private subscription: Subscription;

    private selectedMarkers = [];
    public selectedData;

    public showPreview: boolean;
    public showFilter: boolean;


    //  public dataObservable: Observable<any>;

    constructor(
        private mapService: MapService,
        public toastController: ToastController
    ) {
    }

    ngOnInit() {


        console.log('init ', this.initialSettings.type);

        this.watchPosition();
    }

    private watchPosition() {
        let marker;
        const wait = Geolocation.watchPosition({}, (position, err) => {
            console.log('watch', JSON.stringify(position), position);
            const lat = position.coords.latitude;
            const lng = position.coords.longitude;


            if (!!this.map) {
                if (!!marker) {
                    this.map.removeObjects([marker]);
                }
                marker = this.mapService.setSimpleMarker(this.map, lat, lng, 'assets/familia.svg');
            }
        });

    }

    async getCurrentPosition() {
        const coordinates = await Geolocation.getCurrentPosition();
        console.log('Current', coordinates);
        this.mapService.setPosition(this.map, coordinates.coords.latitude, coordinates.coords.longitude, 14);
    }

    setCurrentPosition(): void {

        Geolocation.getCurrentPosition().then(coordinates => {
            this.mapService.setPosition(this.map, coordinates.coords.latitude, coordinates.coords.longitude, 20);
        });

        // const coordinates = await Geolocation.getCurrentPosition();

    }

    closePreview($event): void {
        this.removeSelectedMarker();
        this.showPreview = false;
    }


    toggleFilterView(): void {
        this.showFilter = !this.showFilter;
    }

    private removeSelectedMarker(): void {
        this.map.removeObjects(this.selectedMarkers);
        this.selectedMarkers = [];
    }


    listenMap(map: any) {


        console.log('map ', map);
        this.map = map;

        this.map.addEventListener('tap', (evt) => {
            // Log 'tap' and 'mouse' events:
            console.log(evt.type, evt.currentPointer.type);
            console.log(evt);

            if (evt.target.Me === null) {
                this.removeSelectedMarker();
                this.closePreview(null);
            }

        });

        this.subscription = this.initialSettings.dataObservable.subscribe(value => {


            console.log('length schools', value.length);
            this.presentToast('Se encontraron ' + value.length + ' resultados');
            this.mapService.buildClusterSchools(this.map, value, this.initialSettings.iconForMarker, (event) => {
                try {
                    const data = event.target.getData().getData();
                    let markerView;

                    this.selectedData = data;
                    this.showPreview = true;

                    this.removeSelectedMarker();

                    markerView = this.mapService.setSimpleMarker(
                        this.map, data.geometry.coordinates[1], data.geometry.coordinates[0], 'assets/view.svg');


                    this.selectedMarkers.push(markerView);

                    console.log(data);
                    //   this.presentToast(data.properties.NOMBRE_EST);

                } catch (e) {
                    console.error('e');
                }


            });


        });


    }

    async presentToast(text) {
        const toast = await this.toastController.create({
            message: text,
            duration: 1000
        });
        toast.present();
    }


    ngOnDestroy() {
        console.log('destroy generic');
        this.subscription.unsubscribe();

    }

    ionViewDidLeave() {
        console.log('leave generic');
    }

}
