export interface ListItem {
    value: string;
    label: string;
}

export const LOCALITIES: ListItem[] = [
    {value: '01', label: 'Usaquén'},
    {value: '02', label: 'Chapinero'},
    {value: '03', label: 'Santa Fe'},
    {value: '04', label: 'San Cristóbal'},
    {value: '05', label: 'Usme'},
    {value: '06', label: 'Tunjuelito'},
    {value: '07', label: 'Bosa'},
    {value: '08', label: 'Kennedy'},
    {value: '09', label: 'Fontibón'},
    {value: '10', label: 'Engativá'},
    {value: '11', label: 'Suba'},
    {value: '12', label: 'Barrios Unidos'},
    {value: '13', label: 'Teusaquillo'},
    {value: '14', label: 'Los Mártires'},
    {value: '15', label: 'Antonio Nariño'},
    {value: '16', label: 'Puente Aranda'},
    {value: '17', label: 'La Candelaria'},
    {value: '18', label: 'Rafael Uribe Uribe'},
    {value: '19', label: 'Ciudad Bolívar'},
    {value: '20', label: 'Sumapaz'},
];
