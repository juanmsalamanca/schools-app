import { NgModule } from '@angular/core';
import { SimpleMapComponent } from './components/simple-map/simple-map.component';
import { MapService } from './services/map.service';


@NgModule({
    exports: [
        SimpleMapComponent
    ],
    providers: [
        MapService
    ],
    declarations: [SimpleMapComponent]
})
export class MapModule {}
