import { AfterViewInit, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { environment } from '../../../../../environments/environment';

declare var H: any;

@Component({
    selector: 'app-simple-map',
    templateUrl: './simple-map.component.html',
    styleUrls: ['./simple-map.component.scss'],
})
export class SimpleMapComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('map')
    public mapElement: ElementRef;

    @Output()
    public getMap = new EventEmitter<any>();

    private platform: any;
    private map: any;

    constructor() {
    }

    ngOnInit() {
        this.platform = new H.service.Platform({
            app_id: environment.here_map_app_id,
            app_code: environment.here_map_app_code
        });
    }

    ngOnDestroy() {
        console.log('destroy map');
        this.map.dispose();
    }

    public ngAfterViewInit() {

        setTimeout(() => {
            const defaultLayers = this.platform.createDefaultLayers();

            this.map = new H.Map(
                this.mapElement.nativeElement,
                defaultLayers.normal.map,
                {
                    zoom: 10,
                    center: {lat: 4.6764511, lng: -74.0652501},
                    pixelRatio: Math.min(devicePixelRatio, 2)
                });

            this.setBaseLayer(this.map, this.platform);

            this.getMap.emit(this.map);


            const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));


            window.addEventListener('resize', () => this.map.getViewPort().resize());
        }, 100);


    }

    setBaseLayer(map, platform) {
        const mapTileService = platform.getMapTileService({
            type: 'base'
        });
        const parameters = {
            ppi: '250'
        };
        const tileLayer = mapTileService.createTileLayer(
            'maptile',
            'normal.day.mobile',
            256,
            'png8',
            parameters
        );
        map.setBaseLayer(tileLayer);
    }

}
