import { Injectable } from '@angular/core';


declare var H: any;


interface DataForDataPoint {
    lat: number;
    lng: number;
    data: object;
}

export interface GeoData {
    properties: {};
    geometry: {
        coordinates: number[]
    };
}


@Injectable({
    providedIn: 'root'
})
export class MapService {

    private clusteringLayer;

    constructor() {

    }


    public setPosition(map: any, lat: number, lng: number, zoom: number = 14): void {
        map.setCenter({lat, lng});
        map.setZoom(zoom);
    }


    public setSimpleMarker(map: any, lat: number, lng: number, iconPath?: string): any {
        const icon = new H.map.Icon(iconPath);
        const marker = new H.map.Marker({lat, lng}, {icon});
        map.addObject(marker);
        return marker;
    }


    public buildClusterSchools(map: any, data: GeoData[], markerImage: string, callbackOnTap): void {

        if (!!this.clusteringLayer) {
            map.removeLayer(this.clusteringLayer);
        }


        const dataForDataPoint = data.filter(x => {
            return x.geometry.coordinates[1] > 2;
        }).map(value => {
            return {
                data: value,
                lng: value.geometry.coordinates[0],
                lat: value.geometry.coordinates[1]
            };
        });

        const dataPoints = this.buildDataPoints(dataForDataPoint);

        const clusteredDataProvider = this.buildClusteredDataProvider(dataPoints, markerImage);

        this.clusteringLayer = new H.map.layer.ObjectLayer(clusteredDataProvider);

        clusteredDataProvider.addEventListener('tap', (event) => {
            callbackOnTap(event);
        });

        map.addLayer(this.clusteringLayer);
    }


    private buildDataPoints(dataForDataPoint: DataForDataPoint[]): any[] {
        return dataForDataPoint.map(value => {
            return new H.clustering.DataPoint(value.lat, value.lng, null, value.data);
        });
    }

    private buildClusteredDataProvider(dataPoints, markerImage: string): any {
        const clusterSvgTemplate = '<svg xmlns="http://www.w3.org/2000/svg" height="{diameter}" width="{diameter}">' +
            '<circle cx="{radius}px" cy="{radius}px" r="{radius}px" fill="#ffc409" />' +
            '<text x="50%" y="50%" font-size="30" text-anchor="middle" stroke="black" stroke-width="2px" dy=".3em">{text}</text>' +
            '</svg>';


        return new H.clustering.Provider(dataPoints, {
            clusteringOptions: {
                eps: 32,
                minWeight: 3
            },
            theme: {
                getClusterPresentation(cluster) {
                    const weight = cluster.getWeight();
                    const radius = 40;
                    const diameter = radius * 2;

                    const svgString = clusterSvgTemplate
                        .replace(/\{radius\}/g, radius + '')
                        .replace(/\{diameter\}/g, diameter + '')
                        .replace(/\{text\}/g, weight + '');

                    const clusterIcon = new H.map.Icon(svgString);
                    const clusterMarker = new H.map.Marker(cluster.getPosition(), {
                        icon: clusterIcon,
                        min: cluster.getMinZoom(),
                        max: cluster.getMaxZoom()
                    });
                    clusterMarker.setData(cluster);
                    return clusterMarker;
                },
                getNoisePresentation(noisePoint) {
                    const noiseIcon = new H.map.Icon(markerImage, {
                        size: {w: 100, h: 100},
                        anchor: {x: 50, y: 50},
                    });
                    const noiseMarker = new H.map.Marker(noisePoint.getPosition(), {
                        icon: noiseIcon,
                        min: noisePoint.getMinZoom(),
                    });
                    noiseMarker.setData(noisePoint);
                    return noiseMarker;
                }
            }
        });
    }


}
