import { NgModule } from '@angular/core';
import { MapModule } from './here-map/map.module';
import { PreviewInfoComponent } from './components/preview-info/preview-info.component';
import { FilterComponent } from './components/filter/filter.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { GenericMapViewComponent } from './generic-map-view/generic-map-view.component';


@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        MapModule,
        PreviewInfoComponent,
        FilterComponent,
        HeaderComponent,
        GenericMapViewComponent
    ],
    declarations: [
        PreviewInfoComponent,
        FilterComponent,
        HeaderComponent,
        GenericMapViewComponent
    ]
})
export class SharedModule {
}
