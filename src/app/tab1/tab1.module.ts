import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { Tab1PageRoutingModule } from './tab1-routing.module';
import { SchoolStoreModule } from '../apps/schools/store/school.store.module';
import { SharedModule } from '../shared/shared.module';
import { PoliceCaisModule } from '../apps/police-cais/police-cais.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        Tab1PageRoutingModule,
        SchoolStoreModule,
        PoliceCaisModule,
        SharedModule
    ],
    declarations: [Tab1Page]
})
export class Tab1PageModule {
}
