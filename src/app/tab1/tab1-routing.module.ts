import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab1Page } from './tab1.page';
import { SchoolMainPage } from '../apps/schools/view/pages/main/school-main.page';
import { SchoolAppModule } from '../apps/schools/school-app.module';
import { PoliceCaisMainPage } from '../apps/police-cais/view/pages/main/police-cais-main.page';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
  },
  {
    path: 'm',
    component: SchoolMainPage
  },
  {
    path: 'police',
    component: PoliceCaisMainPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), SchoolAppModule],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
