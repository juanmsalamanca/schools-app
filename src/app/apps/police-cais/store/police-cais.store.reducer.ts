import { Action, createReducer, on } from '@ngrx/store';
import { errorFetchingPoliceCAISInfo, fetchPoliceCAISInfo, successPoliceCAISInfo } from './police-cais.store.action';
import { PoliceCAISStoreState } from './police-cais.store.state';

export const KEY = 'police-cais';

export const initialState: PoliceCAISStoreState = {
    error: null,
    inProgress: false,
    success: false,
    policeCAIS: []
};

const policeCAISReducer = createReducer(
    initialState,
    on(fetchPoliceCAISInfo, state => ({...state, inProgress: true, success: false, policeCAIS: []})),
    on(errorFetchingPoliceCAISInfo, (state, {error}) => ({
        ...state,
        inProgress: false,
        success: false,
        policeCAIS: [],
        error
    })),
    on(successPoliceCAISInfo, (state, {policeCAIS}) => {
        return ({
            ...state,
            policeCAIS,
            inProgress: false,
            success: true,
            error: null
        });
    }),
);


export function reducer(state: PoliceCAISStoreState | undefined, action: Action) {
    return policeCAISReducer(state, action);
}
