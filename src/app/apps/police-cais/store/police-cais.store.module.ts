import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { KEY, reducer } from './police-cais.store.reducer';
import { PoliceCaisStoreEffect } from './police-cais.store.effect';
import { PoliceCaisStoreFacade } from './police-cais.store.facade';

@NgModule({
    imports: [
        StoreModule.forFeature(KEY, reducer),
        EffectsModule.forFeature([PoliceCaisStoreEffect])

    ],
    providers: [
        PoliceCaisStoreEffect,
        PoliceCaisStoreFacade
    ]
})
export class PoliceCaisStoreModule {
}
