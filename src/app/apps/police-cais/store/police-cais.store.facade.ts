import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';
import { getPoliceCAIS } from './police-cais.store.selector';
import { PoliceCAISStoreState } from './police-cais.store.state';
import { FETCH_POLICE_CAIS_INFO } from './police-cais.store.action';

@Injectable()
export class PoliceCaisStoreFacade {

    getPoliceCAIS$ = this.store.select(getPoliceCAIS);

    constructor(private store: Store<PoliceCAISStoreState>) {
    }

    public fetchPoliceCAIS(): void {
        this.store.dispatch({type: FETCH_POLICE_CAIS_INFO});
    }
}
