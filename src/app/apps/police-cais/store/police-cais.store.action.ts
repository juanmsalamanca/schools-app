import { createAction, props } from '@ngrx/store';

export const FETCH_POLICE_CAIS_INFO = '[Global/API] FETCH_POLICE_CAIS_INFO';
export const SUCCESS_POLICE_CAIS_INFO = '[Global/API] SUCCESS_POLICE_CAIS_INF';
export const ERROR_FETCH_POLICE_CAIS_INFO = '[Global/API] ERROR_FETCH_POLICE_CAIS_INFO';

export const fetchPoliceCAISInfo = createAction(
    FETCH_POLICE_CAIS_INFO,
);

export const successPoliceCAISInfo = createAction(
    SUCCESS_POLICE_CAIS_INFO,
    props<{ policeCAIS: [] }>()
);

export const errorFetchingPoliceCAISInfo = createAction(
    ERROR_FETCH_POLICE_CAIS_INFO,
    props<{ error: string }>()
);
