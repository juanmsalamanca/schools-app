import { createFeatureSelector, createSelector } from '@ngrx/store';
import { KEY } from './police-cais.store.reducer';
import { PoliceCAISStoreState } from './police-cais.store.state';


export const statePoliceCAIS = createFeatureSelector<PoliceCAISStoreState>(KEY);

export const selectPoliceCAIS = createSelector(
    statePoliceCAIS,
    (state) => state.policeCAIS
);

export const getPoliceCAIS = createSelector(
    selectPoliceCAIS,
    (policeCAIS) => {
        return policeCAIS;
    }
);
