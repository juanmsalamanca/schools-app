export interface PoliceCAISStoreState {
    success: boolean;
    error: string;
    inProgress: boolean;
    policeCAIS: [];
}
