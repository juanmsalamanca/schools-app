import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { from, Observable, of } from 'rxjs';
import { ERROR_FETCH_POLICE_CAIS_INFO, FETCH_POLICE_CAIS_INFO, SUCCESS_POLICE_CAIS_INFO } from './police-cais.store.action';


@Injectable()
export class PoliceCaisStoreEffect {

    constructor(private  actions$: Actions) {
    }

    loadPoliceCais$ = createEffect(() => this.actions$.pipe(
        ofType(FETCH_POLICE_CAIS_INFO),
        mergeMap(() => this.getPoliceCais().pipe(
            map(value => ({
                type: SUCCESS_POLICE_CAIS_INFO,
                policeCAIS: value
            })),
            catchError(err => of({
                type: ERROR_FETCH_POLICE_CAIS_INFO,
                message: err
            }))
        ))
    ));


    private getPoliceCais(): Observable<any[]> {
        return from(fetch('assets/data/police-cais-data.json').then(res => res.json()))
            .pipe(map(value => value['features']));
    }

}

