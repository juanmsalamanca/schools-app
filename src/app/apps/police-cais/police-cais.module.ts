import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { PoliceCaisStoreModule } from './store/police-cais.store.module';
import { PoliceCaisMainPage } from './view/pages/main/police-cais-main.page';

@NgModule({
    imports: [
        PoliceCaisStoreModule,
        SharedModule
    ],
    declarations: [
        PoliceCaisMainPage
    ]
})
export class PoliceCaisModule {
}
