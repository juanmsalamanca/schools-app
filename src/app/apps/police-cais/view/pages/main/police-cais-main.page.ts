import { Component } from '@angular/core';
import { InitialSettings } from '../../../../../shared/generic-map-view/generic-map-view.component';
import { PoliceCaisStoreFacade } from '../../../store/police-cais.store.facade';

@Component({
    selector: 'app-police-cais',
    templateUrl: 'police-cais-main.page.html',
    styleUrls: ['police-cais-main.page.scss']
})
export class PoliceCaisMainPage {

    public initialSettings: InitialSettings;


    constructor(private policeCaisStoreFacade: PoliceCaisStoreFacade) {

        const labelsMap = new Map<string, string>();

        labelsMap.set('CAIDESCRIP', 'Nombre');
        labelsMap.set('CAIDIR_SITIO', 'Direcció');
        labelsMap.set('CAITELEFON', 'Telefono');
        labelsMap.set('CAICELECTR', 'Correo');
        labelsMap.set('CAIPWEB', 'Web');

        policeCaisStoreFacade.fetchPoliceCAIS();

        this.initialSettings = {
            title: 'CAIS',
            type: 'CAIS',
            iconForMarker: 'assets/police-station.png',
            mapLabelsInfo: labelsMap,
            dataObservable: policeCaisStoreFacade.getPoliceCAIS$
        };

    }


    /* "CAICOD_PLAN": 5,
     "CAICOD_ENT": "137",
     "CAICOD_PROY": "7507",
     "CAIANIO_GEO": 2016,
     "CAIFECHA_INI": 1464739200000,
     "CAIFECHA_FIN": 1609372800000,
     "CAIDESCRIP": "CAI Codito",
     "CAIEST_PROY": "TERM",
     "CAIINTERV_ESP": "NIES",
     "CAIDIR_SITIO": "KR 4 CL 187A BIS",
     "CAICOD_SITIO": null,
     "CAIHORARIO": "24 Horas",
     "CAITELEFON": "6718230",
     "CAIIUUPLAN": "UPZ9",
     "CAIIUSCATA": "008533",
     "CAIIULOCAL": "01",
     "CAILONGITU": -74.024219000000002,
     "CAILATITUD": 4.7639209999999999,
     "CAITURNO": "24 Horas",
     "CAICELECTR": "mebog.cai-codito@policia.gov.co",
     "CAICONTACT": "Policía Nacional",
     "CAIPWEB": "https://www.policia.gov.co/",
     "CAICPOLICI": "E01-01",
     "CAIIDENTIF": "CAI037",
     "CAIIEPOLIC": "CAI Codito",
     "CAIFECHA_C": 1565654400000*/


}
