import { SchoolStoreState } from './school.store.state';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromSchoolActions from './school.store.action';

export const KEY = 'schools';

export const initialState: SchoolStoreState = {
    schools: [],
    error: null,
    inProgress: false,
    success: false,
    filter: {
        disability: false,
        locality: null
    }
};

const schoolReducer = createReducer(
    initialState,
    on(fromSchoolActions.fetchSchoolInfo, state => ({...state, inProgress: true, success: false, schools: []})),
    on(fromSchoolActions.errorFetchingSchoolInfo, (state, {error}) => ({
        ...state,
        inProgress: false,
        success: false,
        schools: [],
        error
    })),
    on(fromSchoolActions.successSchoolInfo, (state, {schools}) => {
        return ({
            ...state,
            schools,
            inProgress: false,
            success: true,
            error: null
        });
    }),
    on(fromSchoolActions.changeFilter, (state, {filter}) => {
        return ({
            ...state,
            filter
        });
    })
);


export function reducer(state: SchoolStoreState | undefined, action: Action) {
    return schoolReducer(state, action);
}
