import { Injectable } from '@angular/core';
import { FilterI, SchoolStoreState } from './school.store.state';
import { Store } from '@ngrx/store';
import * as fromSchoolActions from './school.store.action';
import * as schoolSelector from './school.store.selector';

@Injectable()
export class SchoolStoreFacade {

    getSchools$ = this.store.select(schoolSelector.getSchools);
    getFilters$ = this.store.select(schoolSelector.getFilters);

    constructor(private store: Store<SchoolStoreState>) {
    }

    public fetchSchools(): void {
        this.store.dispatch({type: fromSchoolActions.FETCH_SCHOOL_INFO});

    }

    public changeFilter(filter: FilterI): void {
        this.store.dispatch({type: fromSchoolActions.CHANGE_FILTER, filter});
    }


}
