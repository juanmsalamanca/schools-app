import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as fromSchoolActions from './school.store.action';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { from, Observable, of } from 'rxjs';

@Injectable()
export class SchoolStoreEffect {

    constructor(private actions$: Actions) {
    }

    loadSchools$ = createEffect(() => this.actions$.pipe(
        ofType(fromSchoolActions.FETCH_SCHOOL_INFO),
        mergeMap(() => this.getSchools()
            .pipe(
                map(value => ({type: fromSchoolActions.SUCCESS_SCHOOL_INFO, schools: value})),
                catchError(err => of({
                    type: fromSchoolActions.ERROR_FETCH_SCHOOL_INFO, message: err
                }))
            ))
    ));

    // TODO: hacer algo con el error
    private getSchools(): Observable<any[]> {
        return from(fetch('assets/data/shools-data.json').then(res => res.json()))
            .pipe(map(value => value['features']));
    }
}
