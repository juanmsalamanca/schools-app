export interface FilterI {
    locality: string;
    disability: boolean;
}

export interface SchoolStoreState {
    success: boolean;
    error: string;
    inProgress: boolean;
    schools: [];
    filter: FilterI;
}
