import { createFeatureSelector, createSelector } from '@ngrx/store';
import { SchoolStoreState } from './school.store.state';


export const stateSchools = createFeatureSelector<SchoolStoreState>('schools');


export const selectSchools = createSelector(
    stateSchools,
    (state) => state.schools
);

export const getFilters = createSelector(
    stateSchools,
    (state) => state.filter
);

export const getSchools = createSelector(
    selectSchools,
    getFilters,
    (schools, filter) => {
        return schools
            .filter(value => filterByLocality(value, filter.locality))
            .filter(value => filterByDisability(value, filter.disability));
    }
);

function filterByLocality(school: any, locality: string): boolean {
    return (!!locality) ? (school['properties']['COD_LOCA'] === locality) : true;
}
const filterByDisability = (school: any, isDisability: boolean): boolean => {

    return (!!isDisability) ? (!!school['properties']['DISCAPACID']) : true;
};

export const filterSchools = createSelector(
    selectSchools,
    (schools, props) => {
        return schools.filter(value => {
            return value['properties']['COD_LOCA'] === props['locality'];
        });
    }
);

export function test(props) {
    return filterSchools;
}
