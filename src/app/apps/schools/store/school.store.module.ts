import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import * as fromSchoolStoreReducer from './school.store.reducer';
import { SchoolStoreFacade } from './school.store.facade';
import { EffectsModule } from '@ngrx/effects';
import { SchoolStoreEffect } from './school.store.effect';

@NgModule({
    imports: [
        StoreModule.forFeature(fromSchoolStoreReducer.KEY, fromSchoolStoreReducer.reducer),
        EffectsModule.forFeature([SchoolStoreEffect])
    ],
    providers: [
        SchoolStoreFacade,
        SchoolStoreEffect
    ]
})

export class SchoolStoreModule {}

