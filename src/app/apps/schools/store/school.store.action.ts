import { createAction, props } from '@ngrx/store';
import { FilterI } from './school.store.state';

export const FETCH_SCHOOL_INFO = '[Global/API] FETCH_SCHOOL_INFO';
export const SUCCESS_SCHOOL_INFO = '[Global/API] SUCCESS_SCHOOL_INFO';
export const ERROR_FETCH_SCHOOL_INFO = '[Global/API] ERROR_FETCH_SCHOOL_INFO';
export const CHANGE_FILTER = '[Global/API] CHANGE_FILTER';

export const fetchSchoolInfo = createAction(
    FETCH_SCHOOL_INFO,
);

export const successSchoolInfo = createAction(
    SUCCESS_SCHOOL_INFO,
    props<{ schools: [] }>()
);

export const errorFetchingSchoolInfo = createAction(
    ERROR_FETCH_SCHOOL_INFO,
    props<{ error: string }>()
);

export const changeFilter = createAction(
    CHANGE_FILTER,
    props<{ filter: FilterI }>()
);
