import { Component } from '@angular/core';
import { InitialSettings } from '../../../../../shared/generic-map-view/generic-map-view.component';
import { SchoolStoreFacade } from '../../../store/school.store.facade';

@Component({
    selector: 'app-schools',
    templateUrl: 'school-main.page.html',
    styleUrls: ['school-main.page.scss']
})
export class SchoolMainPage {

    public initialSettings: InitialSettings;

    constructor(private schoolStoreFacade: SchoolStoreFacade) {

        schoolStoreFacade.fetchSchools();

        const labelsMap = new Map<string, string>();
        labelsMap.set('NOMBRE_EST', 'Nombre');
        labelsMap.set('NIT___DIST', 'Nit');
        labelsMap.set('NOMBRE_SED', 'Sede');
        labelsMap.set('ZONA', 'Zona');
        labelsMap.set('DIRECCION', 'Dirección');
        labelsMap.set('BARRIO__GE', 'Barrio');
        labelsMap.set('TELEFONO', 'Telefono');
        labelsMap.set('EMAIL', 'Correo');
        labelsMap.set('WEB', 'Stio web');
        labelsMap.set('CODIGO_POS', 'Codigo postal');
        labelsMap.set('ENFASIS_PA', 'Enfasis');
        labelsMap.set('DISCAPACID', 'Discapacidades');
        labelsMap.set('TALENTOS_O', 'Talentos');
        labelsMap.set('GRUPOS_ETN', 'Grupos etnicos');
        labelsMap.set('REGIMEN_Y', 'Regimen');
        labelsMap.set('ESTRATO', 'Estrato');

        this.initialSettings = {
            title: 'Colegios',
            type: 'schools',
            iconForMarker: 'assets/school_selected.png',
            mapLabelsInfo: labelsMap,
            dataObservable: schoolStoreFacade.getSchools$
        };


    }


}
