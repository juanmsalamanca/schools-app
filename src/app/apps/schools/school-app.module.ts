import { NgModule } from '@angular/core';
import { SchoolStoreModule } from './store/school.store.module';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { SchoolMainPage } from './view/pages/main/school-main.page';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        SchoolStoreModule,
        SharedModule
    ],
    declarations: [
        SchoolMainPage
    ]
})
export class SchoolAppModule {
}
